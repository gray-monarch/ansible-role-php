gray-monarch.php
=========

Ansible role to install PHP from distribution packages or latest PHP 7 from community accepted repos (remi / dotdeb) (primarily with NGINX in mind right now)

Current features:
-   Choose between default php packages and good third party repos for PHP 7
-   Booleans for the following:
    -   Enabling the service permanently
    -   Installing mysql/pdo components
    -   Set SELinux `httpd_can_network_connect_db` on RHEL/CentOS
    -   Use a `tmpfs` mounted on `/var/lib/php/session[s]` for diskless sessions

Requirements
------------

-   None

Role Variables
--------------

##### defaults/main.yml

|                                 Variable Name | Description                                                                | Default |
|----------------------------------------------:|:---------------------------------------------------------------------------|:--------|
|                   `gm_php_3rd_party_repo_use` | Boolean choice for using Remi/DotDeb's PHP7 repos                          | true    |
|                        `gm_php_tmpfs_size_mb` | Size in MB of the tmpfs (RAM backed)                                       | 100M    |
|                        `gm_php_uninstall_old` | Attempt to purge the system of old php packages before install             | false   |
| `gm_php_selinux_httpd_can_network_connect_db` | Permanently sets seboolean `httpd_can_network_connect_db` to `1` (enabled) | false   |
|               `gm_php_service_enable_php_fpm` | Sets the service to start on boot                                          | true    |
|            `gm_php_install_php_db_components` | Install extra db-related components like `pdo`, `odbc`                     | false   |
|               `gm_php_use_tmpfs_for_sessions` | Boolean to configure tmpfs in `/etc/fstab` and mount (restarts php-fpm)    | true    |

##### vars/[RedHat/Debian].yml

|                    Variable Name | Description                                                                                   | Default                                                                                                         | OS Family |
|---------------------------------:|:----------------------------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------|:----------|
|           `gm_php_dist_dep_pkgs` | Packages that are installed before PHP                                                        | `- epel-release`<br />`- policycoreutils-python`                                                                | RedHat    |
|             `gm_php_php_service` | The name of the PHP service unit (systemd)                                                    | `php-fpm`                                                                                                       | RedHat    |
|                                  |                                                                                               | `php5-fpm` / `php7.0-fpm`                                                                                       | Debian    |
|           `gm_php_dist_php_pkgs` | Distribution package names for their respective php versions                                  | `- php`<br />`- php-fpm`<br />`- php-cli`<br />`- php-opcache`                                                  | RedHat    |
|                                  |                                                                                               | `- php5` / `- php`<br />`- php5-fpm` / `- php-fpm`<br />`- php5-cli` / `- php-cli` <br /> / `- php-opcache`     | Debian    |
|        `gm_php_dist_php_db_pkgs` | PHP packages related to databases                                                             | `- php-pdo`<br />`- php-odbc`                                                                                   | RedHat    |
|                                  |                                                                                               | `- php5-mysql` / `- php-mysql`<br />`- php5-odbc` / `- php-odbc`                                                | Debian    |
|      `gm_php_3rd_party_php_pkgs` | Package collector variable for the main task                                                  | `"{{ gm_php_remi_php_pkgs }}"`                                                                                  | RedHat    |
|                                  |                                                                                               | `"{{ gm_php_dotdeb_php_pkgs }}"`                                                                                | Debian    |
|  `gm_php_3rd_party_php_db_pkgs:` | Package collector variable for the main task                                                  | `"{{ gm_php_remi_php_db_pkgs }}"`                                                                               | RedHat    |
|                                  |                                                                                               | `"{{ gm_php_dotdeb_php_db_pkgs }}"`                                                                             | Debian    |
|  `gm_php_tmpfs_php_session_path` | Filesystem path to where php writes session files                                             | `/var/lib/php/session`                                                                                          | RedHat    |
|                                  |                                                                                               | `/var/lib/php/sessions`                                                                                         | Debian    |
|        `gm_php_tmpfs_fstab_opts` | Mount options for the tmpfs mounting task (differ between os)                                 | `- size={{ gm_php_tmpfs_size_mb }}`<br />`- context="system_u:object_r:httpd_var_run_t:s0"`<br />`- gid=apache` | RedHat    |
|                                  |                                                                                               | `- size={{ gm_php_tmpfs_size_mb }}`                                                                             | Debian    |
|    `gm_php_dotdeb_repo_gpg_name` | Local name of the key file located in the role files dir (for `[rpm/apt]_key`)                | `RPM-GPG-KEY-remi`                                                                                              | RedHat    |
|                                  |                                                                                               | `dotdeb.gpg`                                                                                                    | Debian    |
| `gm_php_dotdeb_repo_definitions` | The variablized definitions of the above respective repositories (for `[yum/apt]_repository`) | See source                                                                                                      | &nbsp;    |

Dependencies
------------

-   None

Example Playbook
----------------

Generic (installs default dist php AND sets it as a persistent service AND uses tmpfs for sessions):

    - hosts: servers
      roles:
        - gray-monarch.php
          gm_php_3rd_party_repo_use: false

Install PHP 7 by default AND allows php to connect to DBs AND installs ODBC/PDO components:

    - hosts: servers
    - roles:
        - gray-monarch.php
          gm_php_selinux_httpd_can_network_connect_db: true
          gm_php_install_php_db_components: true


License
-------

Apache 2.0

Author Information
------------------

Michael Goodwin <mike@contactvelocity.com>
